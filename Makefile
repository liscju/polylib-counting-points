all: main

clean:
	rm *.o
	rm main

main: main.c
	gcc -c -g -O2 -I /usr/local/include/polylib/ -DLINEAR_VALUE_IS_INT main.c -o main.o
	gcc main.o /usr/local/lib/libpolylib64.a -lpthread -o main

